function HebDate(props) {
  return (
  <Page>
      <Section
        title={<Text bold align="center">About the HebDate Clock</Text>}>
        <Text>The HebDate clock face provides access to core activity stats, along with the current date in both the Gregorian and Hebrew calendars. The bottom half of the clock face provides notification of events on the Hebrew calendar and the candle lighting time for your location.</Text>
        <Text>This clock face is free to use.  If you like my content and you want to donate to my creative efforts, you can make a voluntary contribution at:  
        <Link source="https://ko-fi.com/tohovevohu">https://ko-fi.com/tohovevohu</Link></Text>
      </Section>
      <Section
        title={<Text bold align="center">HebDate Clock Settings</Text>}>
        <Text>Choose the color palette base for the watch face</Text>
        <ColorSelect
          settingsKey="colorScheme"
          colors={[
            {color: '#D32F2F'},
            {color: '#FFA000'},
            {color: '#0097A7'},
            {color: '#1976D2'},
            {color: '#303F9F'}
          ]}
        />
        <Text>Show the date for after sunset on today's Gregorian date</Text>
        <Toggle
          settingsKey="sunset"
          label="After sunset" />
      </Section>
      <Section
        title={<Text bold align="center">Acknowledgements and Credits</Text>}>
        <Text>This app makes use of icons from <Link source="https://icons8.com">Icons8</Link></Text>
        <Text>Hebrew calendar information is obtained through the opensource <Link source="https://www.hebcal.com/">HebCal project</Link></Text>
      </Section>
   </Page>
  );
}

registerSettingsPage(HebDate);