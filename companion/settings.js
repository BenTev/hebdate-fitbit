import * as messaging from "messaging";
import { settingsStorage } from "settings";
import * as palettes from "../resources/palettes.js";

export function initialize() {
  settingsStorage.addEventListener("change", evt => {
    if (evt.oldValue !== evt.newValue) {
      sendValue(evt.key, evt.newValue);
    }
  });
}

export function getSunsetKey() {
  return settingsStorage.getItem("sunset");
}

function sendValue(key, val) {
  if (val) {
    let data;
    if (val == '"#FFA000"') {
      data = palettes.AMBER_BROWN;
    }
    if (val == '"#1976D2"') {
      data = palettes.BLUE_BLUEGREY;
    }
    if (val == '"#0097A7"') {
      data = palettes.CYAN_LIGHTGREEN;
    }
    if (val == '"#303F9F"') {
      data = palettes.INDIGO_GREY;
    }
    if (val == '"#D32F2F"') {
      data = palettes.RED_PURPLE;
    }
    sendSettingData({
      key: key,
      value: data
    });
  }
}

function sendSettingData(data) {
  if (messaging.peerSocket.readyState === messaging.peerSocket.OPEN) {
    messaging.peerSocket.send(data);
  } else {
    console.log("No peerSocket connection");
  }
}