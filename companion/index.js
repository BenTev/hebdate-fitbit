import * as messaging from "messaging";
import * as util from "../common/utils";
import geolocation from "geolocation";
import { me as companion } from "companion";
import * as settings from "./settings.js";

// API Key for location service
const TIMEZONEDB_KEY = "181M3C0ICS5C"; 

// Initialize the settings listener
settings.initialize();

// function to return a JSON object from an API call 
function fetchFromHebCal(dateString, timeString){
    console.log("Fetching " + dateString);
    fetch(dateString)
    .then(function(response){
        if(response.ok){
            response.json()
            .then(function(data){
                console.log("Fetching: " + timeString);
                fetch(timeString)
                .then(function(timeResponse){
                  if(timeResponse.ok){
                    timeResponse.json()
                    .then(function(timeData){
                      data.items=[];
                      let today = new Date();
                      for (let item in timeData.items){
                        let dateString = timeData.items[item].date;
                        if(!dateString.includes("T")){
                          dateString = dateString + "T00:00:00" + util.prettifyTzo(today.getTimezoneOffset()) + ":00";
                        }
                        let itemDate = new Date(dateString);
                        if (itemDate.getDate() == today.getDate() && itemDate.getMonth() == today.getMonth() && itemDate.getFullYear() == today.getFullYear()) {
                          if (timeData.items[item].category != "parashat"){
                            data.items.push({"category":timeData.items[item].category, "title":timeData.items[item].title});
                          }
                        }
                      }
                      if (today.getDay() == 5) {
                        let parashat = ""
                        for (let event in data.events){
                          if (data.events[event].includes("Parashat")) {
                            parashat = data.events[event];
                            let endStr = parashat.slice(parashat.indexOf("Parashat "), parashat.length);
                            endStr = endStr.slice("Parashat ".length, endStr.length);
                            let begStr = parashat.slice(0, parashat.indexOf("Parashat "));
                            parashat = begStr + endStr;
                          }
                        }
                        
                        data.items.push({"category":"shabbat", "title":"Shabbat " + parashat});
                      }
                      data.type="hebDate";
                        returnData(data);
                      
                    })
                  }
                  else {
                    console.log("Error with response for Shabbat times:", response.status);
                  }
                })
            })
        }
    })
}

// Get the timezone id with an API call
function fetchTimeZoneId(apiString, lat, long){
  console.log("Fetching location data from: " + apiString);
  fetch(apiString)
  .then(function(response){
    if(response.ok){
      console.log("Received data from: " + apiString);
      response.json()
      .then(function(data){
        for (let key in data){
          if(key != "zoneName"){
            delete data[key];
          }
        }
        data.type="location";
        data.lat = lat;
        data.long = long;
        returnData(data);
      })
    }
    else {
      console.log("Error retrieving data from: " + apiString);
    }
  })
}

// Get the geolocation from the device
function getLocationData(){
  geolocation.getCurrentPosition(locationSuccess, locationError, {
    timeout: 60 * 1000
    });
}

// Handle successful call to geolocation
function locationSuccess(position) {

  let lat = position.coords.latitude;
  let long = position.coords.longitude;
  let apiString = "https://api.timezonedb.com/v2.1/get-time-zone?key=" + 
      TIMEZONEDB_KEY +
      "&format=json&by=position&lat=" +
      lat +
      "&lng=" +
      long;
  fetchTimeZoneId(apiString, lat, long);
  
}

// Handle failed call to geolocation
function locationError(error) {
  console.log("Error: " + error.code, "Message: " + error.message);
}

function fetchDate(dateString){
  console.log("Fetching " + dateApiString);
  fetch(dateApiString)
  .then(function(response){
    if(response.ok){
    response.json()
    .then(function(data){
      messaging.peerSocket.onopen = function() {
        returnDate(data);
      }
    })
    }
  })
}

function returnData(data){
  console.log("Returning data: " + JSON.stringify(data));
  if (messaging.peerSocket.readyState === messaging.peerSocket.OPEN) {
    messaging.peerSocket.send(data);
  }
  else {
    console.log("Error:  Socket not open");
  }
}


function setDateApiString(today) {
  let day = today.getDate();
  let year = today.getFullYear();
  let month = today.getMonth()+1;

  let apiString = "https://www.hebcal.com/converter/?cfg=json&gy=" +
    year + 
    "&gm=" +
    month +
    "&gd=" +
    day + 
    "&g2h=1"
  if (settings.getSunsetKey() == "true") {
    apiString = apiString + "&gs=on";
  }
  return apiString;
}

function setTimeApiString(lat, long, zoneName) {
  let apiString= "https://www.hebcal.com/shabbat/?cfg=json&geo=pos&latitude=" +
     lat + 
     "&longitude=" + 
     long + 
     "&tzid=" +
     zoneName + 
     "&m=50&b=18&leyning=off";
  return apiString;
}

function prepareToFetch(lat, long, zoneName){
  let today = new Date();
  let dateString = setDateApiString(today);
  let timeString = setTimeApiString(lat, long, zoneName);
  fetchFromHebCal(dateString, timeString);
}

messaging.peerSocket.onmessage = function(evt){
  console.log("Message received from app");
  if (evt.data && evt.data.command == 'hebDate') {
    // date requested
    prepareToFetch(evt.data.lat, evt.data.long, evt.data.zoneName);
  }
  if (evt.data && evt.data.command == 'location') {
    // location requested
    getLocationData();
  }
}

// on start, watch for significant location changes

if (
  !companion.permissions.granted("access_location") ||
  !companion.permissions.granted("run_background")
) {
  console.error("We're not allowed to access to GPS Position!");
}

// Monitor for significant changes in physical location
companion.monitorSignificantLocationChanges = true;

// Listen for the event
companion.addEventListener("significantlocationchange", doThis);

// Event happens if the companion is launched and has been asleep
if (companion.launchReasons.locationChanged) {
  doThis(companion.launchReasons.locationChanged.position);
}

function doThis(position) {
  console.log(`Significant location change! ${JSON.stringify(position)}`);
  locationSuccess(position);
}