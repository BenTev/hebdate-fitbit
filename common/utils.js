// Constant to relate month indices to month names
export const months = {
  0 : "Jan",
  1 : "Feb",
  2 : "Mar",
  3 : "Apr",
  4 : "May",
  5 : "Jun",
  6 : "Jul",
  7 : "Aug",
  8 : "Sep",
  9 : "Oct",
  10 : "Nov",
  11 : "Dec"
}

// Constant to relate day indices to day names
export const days = {
  0 : "Sun",
  1 : "Mon",
  2 : "Tue",
  3 : "Wed",
  4 : "Thu",
  5 : "Fri",
  6 : "Sat"
}

// Add zero in front of numbers < 10
export function zeroPad(i) {
  if (i < 10) {
    i = "0" + i;
  }
  return i;
}

// Function to pretty format time zone offsets
export function prettifyTzo(tzo) {
  tzo = zeroPad(tzo/60);
  tzo = "-"+tzo;
  return tzo;
}
