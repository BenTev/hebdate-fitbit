import clock from "clock";
import document from "document";
import { preferences } from "user-settings";
import * as util from "../common/utils";
import * as fs from "fs";
import * as apiServices from "./apiServices.js";
import * as power from "./power.js";
import * as stats from "./stats.js";
import * as settings from "./settings.js";

// Initialize fetching procedures
apiServices.startApiCalls();

// Initialize stat monitoring
stats.startStatMonitoring();

// Set up globals for events and status tracking
let eventList = [];
let eventCounter = 0;
let clockCentered = false;

// Get the Hebrew calendar information right away
refreshHebDate;

// Update the clock every second
clock.granularity = "seconds";

// Get a handle on the elements
const time = document.getElementById("time");
const gregDate = document.getElementById("gregDate");
const hebDate = document.getElementById("hebDate");
const candles = document.getElementById("candles");
const candleTimes = document.getElementById("candleTimes");
const eventTopLine = document.getElementById("eventTopLine");
const event = document.getElementById("event");
const eventBotLine = document.getElementById("eventBotLine");
const clockDisplay = document.getElementById("clockDisplay");

// Get collections of elements by class for color setting application
const darkPrimary = document.getElementsByClassName("dark-primary-color");
const defaultPrimary = document.getElementsByClassName("default-primary-color");
const lightPrimary = document.getElementsByClassName("light-primary-color");
const textPrimary = document.getElementsByClassName("text-primary-color");
const accent = document.getElementsByClassName("accent-color");
const primaryText = document.getElementsByClassName("primary-text-color");
const secondaryText = document.getElementsByClassName("secondary-text-color");
const divider = document.getElementsByClassName("divider-color");

// Clock function to update display on the second
clock.ontick = (evt) => {
  let today = evt.date;
  let hours = today.getHours();
  let ampm = "AM";
  if (preferences.clockDisplay === "12h") {
    // 12h format
    if (hours >= 12){
      ampm = "PM"
    }
    hours = hours % 12 || 12;
  } else {
    // 24h format
    ampm = "";
    hours = util.zeroPad(hours);
  }
  let mins = util.zeroPad(today.getMinutes());
  time.text = `${hours}:${mins} ${ampm}`;
  
  // Set the day, month and date for today
  let day = util.days[today.getDay()];
  let month = util.months[today.getMonth()];
  let date = today.getDate();
  
  // display format for the Gregorian date
  gregDate.text = `${day} ${date} ${month}`
  
  if (evt.date.getSeconds() % 5 == 0) {
    // Refresh the Hebrew date based on the stored data every 5 seconds
    refreshHebDate();
  }
  
  // cycle to the next event in the list every 3 seconds
  if (evt.date.getSeconds() % 3 == 0) {
    if (eventList.length > 0) {
      eventTopLine.style.display = "inline";
      eventBotLine.style.display = "inline";
      eventCounter = (eventCounter + 1) % eventList.length;
      event.text = eventList[eventCounter];
    }
    // if there are no events, hide the display elements for events
    else {
      eventTopLine.style.display = "none";
      eventBotLine.style.display = "none";
      event.text = "";
    }
  }
  
  // Update stats
  power.getCurrentPowerLevel();
  stats.updateActivity();
}

// Callback function for settings changes
function settingsCallback(data) {
  if (!data){
    console.log("No settings data received")
    return;
  }
  if (data.colorScheme){
    console.log(JSON.stringify(data.colorScheme));
    for (let element in darkPrimary) {
      darkPrimary[element].style.fill = data.colorScheme.darkPrimary;
    }
    for (let element in defaultPrimary) {
      defaultPrimary[element].style.fill = data.colorScheme.defaultPrimary;
    }
    for (let element in lightPrimary) {
      lightPrimary[element].style.fill = data.colorScheme.lightPrimary;
    }
    for (let element in textPrimary) {
      textPrimary[element].style.fill = data.colorScheme.textPrimary;
    }
    for (let element in accent) {
      accent[element].style.fill = data.colorScheme.accent;
    }
    for (let element in primaryText) {
      primaryText[element].style.fill = data.colorScheme.primaryText;
    }
    for (let element in secondaryText) {
      secondaryText[element].style.fill = data.colorScheme.secondaryText;
    }
    for (let element in divider) {
      divider[element].style.fill = data.colorScheme.divider;
    }
  }
}
settings.initialize(settingsCallback);

// Function to check the currency of the files for the Hebrew date and events, and update if necessary
function refreshHebDate() {
  let hebDateString = fs.readFileSync("hebDateText.txt", "utf-8");
  let hebEventJson = fs.readFileSync("hebEventJson.txt", "json");
  hebDate.text = hebDateString;
  
  let candleVis = "none";
  let candleText = "";
  let newEventList = [];
  
  for (let item in hebEventJson) {
    if (hebEventJson[item].category == "candles") {
      candleVis = "inline";
      candleText = hebEventJson[item].title;
      let splitPosition = candleText.indexOf(":") + 1;
      candleText = candleText.slice(splitPosition, candleText.length);
    }
    else {
      newEventList.push(hebEventJson[item].title);
    }
  }

  // Move the date and time display up to make room for events when necessary
  if (newEventList.length > 0 && clockCentered) {
    clockDisplay.groupTransform.translate.y=-75;
    clockCentered = false;
  } else if (!clockCentered && newEventList.length < 1) {
    clockDisplay.groupTransform.translate.y=75;
    clockCentered = true;
  }

  candles.style.display = candleVis;
  candleTimes.text = candleText;
  eventList = newEventList;
}

