import { battery } from "power";
import document from "document";

const lowBattery = document.getElementById("lowBattery");
const medBattery = document.getElementById("medBattery");
const highBattery = document.getElementById("highBattery");


export function getCurrentPowerLevel() {
  let currentLevel = Math.floor(battery.chargeLevel);
  if (currentLevel > 70) {
    highBattery.style.display = "inline";
    medBattery.style.display = "inline";
    lowBattery.style.display = "inline";
    highBattery.style.fill = "forestgreen";
    medBattery.style.fill = "forestgreen";
    lowBattery.style.fill = "forestgreen";
  }
  else if (currentLevel > 30) {
    highBattery.style.display = "none";
    medBattery.style.display = "inline";
    lowBattery.style.display = "inline";
    highBattery.style.fill = "gold";
    medBattery.style.fill = "gold";
    lowBattery.style.fill = "gold";
  }
  else {
    highBattery.style.display = "none";
    medBattery.style.display = "none";
    lowBattery.style.display = "show";
    highBattery.style.fill = "red";
    medBattery.style.fill = "red";
    lowBattery.style.fill = "red";
  }
}