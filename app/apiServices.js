import * as messaging from "messaging";
import * as fs from "fs";
import * as settings from "./settings.js";

// Keep the stored date as a variable in this script.

let storedDate = "";
if(fs.existsSync('gregDateText.txt')){
  storedDate = fs.readFileSync('gregDateText.txt', 'utf-8');
}

export function startApiCalls(){
  messaging.peerSocket.onopen = () => {
    locationApiCall();
    hebrewDateApiCall();
    }
  
  // fetch location every 10 minutes
  // setInterval(locationApiCall, 10 * 1000 * 60);
  // fetch Hebrew Date information every 1 minute, if required
  setInterval(hebrewDateApiCall, 1 * 1000 * 60);
}

// Handle messaging errors cleanly
messaging.peerSocket.onerror = (err) => {
  console.log(`Connection error: ${err.code} - ${err.message}`);
}

// Handle incoming messages from the companion app
messaging.peerSocket.onmessage = (evt) => {
  /*
  If the event type is set to 'location', compare the current stored location parameters to the updated location from the companion
  If they are not the same, write the new location to the file for later recall
  */
  if (evt.data && evt.data.type == "location"){
    let locationData = getLocationData();
    let lat = locationData.lat;
    let long = locationData.long;
    let zoneName = locationData.zoneName;
    if (lat != evt.data.lat || long != evt.data.long || zoneName != evt.data.zoneName){
      console.log("location changed!");
      fs.writeFileSync('locationData.txt', evt.data, 'json');
      console.log("Wrote to location file: " + JSON.stringify(evt.data));
      apiCall('hebDate');
    }
  }
  /*
  If the event type is 'hebDate', then overwrite the current files for storing dates
  */
  else if (evt.data && evt.data.type == "hebDate") {
    let hebDate = evt.data.hd + " " + evt.data.hm + " " + evt.data.hy;
    storedDate = evt.data.gd + " " + evt.data.gm + " " + evt.data.gy;
    fs.writeFileSync('hebDateText.txt', hebDate, 'utf-8');
    fs.writeFileSync('gregDateText.txt', storedDate, 'utf-8');
    fs.writeFileSync('hebEventJson.txt', evt.data.items, 'json');
    console.log("Wrote to Hebrew date file: " + hebDate);
    console.log("Wrote to Gregorian date file: " + storedDate);
    console.log("Wrote to events file: " + JSON.stringify(evt.data.items));
  }
  /* 
  If the event type is 'colorScheme', update the settings to the new color scheme
  */
  else if (evt.data && evt.data.key == "colorScheme") {
    settings.setColorScheme(evt.data);
  }
  /* 
  If the event type is "sunset", the setting on whether to get the date before or after sunset has changed.
  Refresh the date to account for this.
  */
  else if (evt.data && evt.data.key == "sunset") {
    apiCall("hebDate");
  }
}

// Handle all api calls
function apiCall(apiName) {
  // create a data object to be returned, and assign the command element to be the parameter passed in
  let data = {command : apiName};
  /* 
  If the api call is to update the location, no further data is needed and the message can be sent
  */
  if (apiName == "location") {
    if (messaging.peerSocket.readyState === messaging.peerSocket.OPEN) {
      messaging.peerSocket.send(data);
    }
    else {
      console.log("Socket not open");
    }
  }
  else if (apiName == "hebDate") {
    if (messaging.peerSocket.readyState === messaging.peerSocket.OPEN) {
      let locationData = getLocationData();
      data.lat = locationData.lat;
      data.long = locationData.long;
      data.zoneName = locationData.zoneName;
      messaging.peerSocket.send(data);
    }
    else {
      console.log("Socket not open");
    }
  }
}

function locationApiCall() {
  apiCall('location');
}

function hebrewDateApiCall() {
  let today = new Date();
  let todayDate = today.getDate();
  let todayMonth = today.getMonth() + 1;
  let todayYear = today.getFullYear();
  let todayDateString = todayDate + " " + todayMonth + " " + todayYear;
  if (todayDateString != storedDate){
    apiCall('hebDate');
  } 
}

function getLocationData(){
  let locationData;
  try {
      locationData= fs.readFileSync("locationData.txt", "json");
    } catch(e){
      console.log("Could not read location data: ", e);
      // Use default location of Jerusalem for location data
      locationData=JSON.parse('{"zoneName":"Asia/Jerusalem", "type":"location","lat":31.768318, "long":35.213711}');
    }
  return locationData;
}