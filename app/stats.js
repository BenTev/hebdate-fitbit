import * as util from "../common/utils.js";
import { HeartRateSensor } from "heart-rate";
import { display } from "display";
import document from "document";
import { me as appbit } from "appbit";
import { today } from "user-activity";
import { goals } from "user-activity";

// get handles for gauges
const heartGauge = document.getElementById("heartGauge");
const stepsGauge = document.getElementById("stepsGauge");
const amGauge = document.getElementById("amGauge");
const calorieGauge = document.getElementById("calorieGauge");

const heartText = document.getElementById("heartText");
const stepsText = document.getElementById("stepsText");
const amText = document.getElementById("amText");
const calorieText = document.getElementById("calorieText");

export function startStatMonitoring(){
  
  if (HeartRateSensor && appbit.permissions.granted("access_heart_rate")) {
    const hrm = new HeartRateSensor();
    hrm.addEventListener("reading", () => {
      heartGauge.sweepAngle = 360 * (hrm.heartRate / 200);
      heartText.text = hrm.heartRate;
    });
    display.addEventListener("change", () => {
      // Automatically stop the sensor when the screen is off to conserve battery
      display.on ? hrm.start() : hrm.stop();
    });
    hrm.start();
  } else {
    console.log("Cannot access heart rate monitor - does not exist or permission not granted");
    heartGauge.sweepAngle = 0;
    heartText.text = "-";
  }
}

export function updateActivity() {
  if (appbit.permissions.granted("access_activity") && display.on) {
    let stepGoal = goals.steps;
    let amGoal = goals.activeMinutes;
    let calorieGoal = goals.calories;
    
    stepsGauge.sweepAngle = 360 * (today.adjusted.steps / stepGoal);
    amGauge.sweepAngle = 360 * (today.adjusted.activeMinutes / amGoal);
    calorieGauge.sweepAngle = 360 * (today.adjusted.calories / calorieGoal);
    
    stepsText.text = today.adjusted.steps;
    amText.text = today.adjusted.activeMinutes;
    calorieText.text = today.adjusted.calories;
  }
  else {
    console.log("Cannot access stats, permission not granted");
  }
}


