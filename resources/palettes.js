export const AMBER_BROWN = JSON.parse('{' +
  '"darkPrimary" : "#FFA000", '+
  '"defaultPrimary" : "#FFC107", '+
  '"lightPrimary" : "#FFECB3", '+
  '"textPrimary" : "#FFFFFF", '+
  '"accent" : "#795548", '+
  '"primaryText" : "#212121", '+
  '"secondaryText" : "#757575", '+
  '"divider" : "#BDBDBD" '+
'}');

export const BLUE_BLUEGREY = JSON.parse('{'+
  '"darkPrimary" : "#1976D2", '+
  '"defaultPrimary" : "#2196F3", '+
  '"lightPrimary" : "#BBDEFB", '+
  '"textPrimary" : "#FFFFFF", '+
  '"accent" : "#607D8B", '+
  '"primaryText" : "#212121", '+
  '"secondaryText" : "#757575", '+
  '"divider" : "#BDBDBD" '+
'}');

export const CYAN_LIGHTGREEN = JSON.parse('{'+
  '"darkPrimary" : "#0097A7", '+
  '"defaultPrimary" : "#00BCD4",'+
  '"lightPrimary" : "#B2EBF2", '+
  '"textPrimary" : "#FFFFFF", '+
  '"accent" : "#88C34A", '+
  '"primaryText" : "#212121", '+
  '"secondaryText" : "#757575",'+
  '"divider" : "#BDBDBD" '+
'}');

export const INDIGO_GREY = JSON.parse('{'+
  '"darkPrimary" : "#303F9F",'+
  '"defaultPrimary" : "#3F51B5",'+
  '"lightPrimary" : "#C5CAE9",'+
  '"textPrimary" : "#FFFFFF",'+
  '"accent" : "#9E9E9E",'+
  '"primaryText" : "#212121",'+
  '"secondaryText" : "#757575",'+
  '"divider" : "#BDBDBD"'+
'}');

export const RED_PURPLE = JSON.parse('{'+
  '"darkPrimary" : "#D32F2F", '+
  '"defaultPrimary" : "#F44336",'+
  '"lightPrimary" : "#FFCDD2",'+
  '"textPrimary" : "#FFFFFF",'+
  '"accent" : "#E040FB",'+
  '"primaryText" : "#212121",'+
  '"secondaryText" : "#757575",'+
  '"divider" : "#BDBDBD"'+
'}');